package = "moonloader-jsoncfg2"
version = "1.0.0-1"
source = {
   url = "git+https://gitlab.com/Pakulichev/moonloader-jsoncfg2.git",
   tag = "v.1.0.0"
}
description = {
   summary = "jsoncfg4moonloader",
   homepage = "https://gitlab.com/Pakulichev/moonloader-jsoncfg2",
   license = "MIT"
}
dependencies = {
   "lua >= 5.1, < 5.4"
}
build = {
   type = "builtin",
   modules = {}
}
